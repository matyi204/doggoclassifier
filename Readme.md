# Dog classifier

This project illustrates how to train an image classifier, implemented with a deep neural network, using fast.ai's library, and how to deploy it to be accessible as a web endpoint from a simplified client.

## Built With

* [miniconda](https://docs.conda.io/en/latest/miniconda.html) - Package, dependency and environment management for python (among others)
* [fastai](https://www.fast.ai/) - neural net library used to train and run the classifier
* [Flask](https://flask.palletsprojects.com/en/1.1.x/) - a python micro web framework, used to make the classifier accessible to web clients
* [node.js/npm](https://nodejs.org/en/) - Javascript runtime environment and package manager, used for the implementation of the sample web client
* [Nuxt.js](https://nuxtjs.org/) - a javascript web framework, used to quickly prototype a Vue.js based web client

### Getting started

A step by step series of examples that tell you how to get a development env running

#### Training
* Install miniconda

* Open an anaconda command prompt/powershell

* Create a new virtual python 3.7 environment 
    ```
    conda create -n dog_classifier python=3.7 anaconda
    ```
* Activate the environment
    ```
    conda activate dog_classifier
    ```
* Install fast.ai and its dependency, pytorch
    ```
    conda install -c pytorch -c fastai fastai
    ```
* Create and navigate into the directory you want to checkout the project
    ```
    mkdir dog_classifier
    cd dog_classifier
    ```
* Clone this repository
    ```
    git clone https://gitlab.com/matyi204/doggoclassifier.git .
    ```
* Create a folder for your dataset
    ```
    mkdir data
    ```
* Download and extract the dataset, this project uses the [Dog breed identification](https://www.kaggle.com/c/dog-breed-identification/data) dataset
* Start jupyter notebook and open in it the *Training.ipynb* file
    ```
    jupyter notebook
    ```
* Experiment around, run the steps to train a neural network using resnet34 and resnet50 architectures

#### Deployment
* Install [node.js/npm](https://nodejs.org/en/)
* Open a powershell and navigate to the folder containing the client application
    ```
    cd dog_classifier/client
    ```
* Install the required javascript packages
    ```
    npm install
    ```
* Start the development server
    ```
    npm run dev
    ```
* With the client running, start a conda shell for the server and install Flask
    ```
    conda activate dog_classifier
    cd dog_classifier/server
    pip install Flask
    ```
* Export the filename which points to the server, then start the flask server
    ```
    export FLASK_APP=main.py
    flask run
    ```
* With this you should have a Vue.js javascript client, which allows to upload an image to the API implemented using Flask. As a response, the API will return the top 3 highest scoring predictions.
